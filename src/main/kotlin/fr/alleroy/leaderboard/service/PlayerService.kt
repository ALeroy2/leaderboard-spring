package fr.alleroy.leaderboard.service

import fr.alleroy.leaderboard.domain.repositorries.PlayerRepository
import fr.alleroy.leaderboard.rest.model.PlayerDto
import fr.alleroy.leaderboard.rest.model.SavePlayerDto
import fr.alleroy.leaderboard.service.mapper.PlayerMapper
import fr.alleroy.leaderboard.util.logger
import org.springframework.stereotype.Service

@Service
class PlayerService(
    val playerRepository: PlayerRepository
) {
    var log = logger()

    fun getPlayer(id: Long): PlayerDto {
        log.info("search patient by id: $id")
        return playerRepository.getReferenceById(id).let { PlayerMapper.toDto(it) }
    }


    fun createPlayer(savePlayer: SavePlayerDto): PlayerDto {
        val playerDto = PlayerDto(
            pseudo = savePlayer.pseudo,
            highScore = savePlayer.score
        )
        val player = PlayerMapper.toEntity(playerDto)
        return PlayerMapper.toDto(playerRepository.save(player))
    }
}

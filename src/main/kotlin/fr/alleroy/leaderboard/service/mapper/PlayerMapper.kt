package fr.alleroy.leaderboard.service.mapper

import fr.alleroy.leaderboard.domain.entities.Player
import fr.alleroy.leaderboard.rest.model.PlayerDto

object PlayerMapper {
    fun toDto(entity: Player): PlayerDto {
        return PlayerDto(
            pseudo = entity.pseudo,
            highScore = entity.highScore,
            scores = entity.scores
        )
    }

    fun toEntity(dto: PlayerDto): Player {
        return Player(
            pseudo = dto.pseudo,
            highScore = dto.highScore,
            scores = dto.scores
        )
    }
}

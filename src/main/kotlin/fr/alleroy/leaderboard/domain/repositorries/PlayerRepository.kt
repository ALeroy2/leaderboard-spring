package fr.alleroy.leaderboard.domain.repositorries

import fr.alleroy.leaderboard.domain.entities.Player
import org.springframework.data.jpa.repository.JpaRepository

interface PlayerRepository: JpaRepository<Player,Long>

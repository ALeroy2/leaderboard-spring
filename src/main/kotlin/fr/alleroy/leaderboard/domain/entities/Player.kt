package fr.alleroy.leaderboard.domain.entities

import jakarta.persistence.*
import org.springframework.data.jpa.domain.support.AuditingEntityListener

@Entity
@Table
@EntityListeners(AuditingEntityListener::class)
class Player(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long? = null,
    @Column(nullable = false)
    val pseudo: String,
    @Column(nullable = false)
    val highScore: Int,
    @OneToMany(targetEntity = Score::class, mappedBy = "player", cascade = [CascadeType.ALL])
    val scores: MutableList<Score> = mutableListOf()
)

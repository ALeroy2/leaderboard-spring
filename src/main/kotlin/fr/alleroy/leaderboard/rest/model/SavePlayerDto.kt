package fr.alleroy.leaderboard.rest.model

data class SavePlayerDto(
    val pseudo: String,
    val score: Int
)

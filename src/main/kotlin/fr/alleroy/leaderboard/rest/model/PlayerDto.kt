package fr.alleroy.leaderboard.rest.model

import fr.alleroy.leaderboard.domain.entities.Score

data class PlayerDto(
    val pseudo: String,
    val highScore: Int,
    val scores: MutableList<Score> = mutableListOf()
)

package fr.alleroy.leaderboard.rest

import fr.alleroy.leaderboard.rest.model.PlayerDto
import fr.alleroy.leaderboard.rest.model.SavePlayerDto
import fr.alleroy.leaderboard.service.PlayerService
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@Validated
@RequestMapping("api/players")
class PlayerController(
    val playerService: PlayerService
) {

    @PostMapping
    @Operation(summary = "create a new player", operationId = "createPlayer")
    @ApiResponses(
        ApiResponse(responseCode = "200", description = "Successfull"),
        ApiResponse(responseCode = "401", description = "Unauthorized")
    )
    fun createPlayer(@RequestBody savePlayer: SavePlayerDto): PlayerDto {
        return playerService.createPlayer(savePlayer)
    }

    @GetMapping(value = ["/{id}"])
    @Operation(summary = "Get one player by his id", operationId = "getPlayerById")
    @ApiResponses(
        ApiResponse(responseCode = "200", description = "Successful"),
        ApiResponse(responseCode = "401", description = "Unauthorized")
    )
    fun getPlayer(@PathVariable id: Long): PlayerDto {
        return playerService.getPlayer(id)
    }
}

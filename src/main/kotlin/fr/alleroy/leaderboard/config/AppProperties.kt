package fr.alleroy.leaderboard.config

import org.springframework.boot.autoconfigure.security.SecurityProperties
import org.springframework.boot.context.properties.ConfigurationProperties
import java.time.Duration

@ConfigurationProperties(value = "explorer")
data class AppProperties(
    val security: SecurityProperties
)

data class SecurityProperties(
    val encodePassword: String,
    val jwt: JwtProperties
)

data class JwtProperties(
    val override: Boolean = false,
    val maxExpiration: Duration = Duration.parse("1h")
)

# leaderboard-spring

## Requirements

- JDK 17
- Docker

## Running the service

### Prepare
### Run the service

The project is built with Gradle. A gradle wrapper is included in the project.
To run the service, execute command:
- Unix system
```shell
./gradlew bootRun
```

- Windows
```shell
./gradlew.bat bootRun
```
The service expose API at url http://localhost:8080

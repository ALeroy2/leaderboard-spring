FROM eclipse-temurin:17-jre-alpine

ADD build/libs/inter-bilan-backend-1.0.0-SNAPSHOT-boot.jar ./app.jar

#  The app must start only after PostgreSQL is fully initialized.
CMD sleep ${APP_START_DELAY} \
    && java ${JAVA_OPTS} -jar /app.jar
